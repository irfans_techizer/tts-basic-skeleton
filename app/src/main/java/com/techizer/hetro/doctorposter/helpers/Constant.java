package com.techizer.hetro.doctorposter.helpers;

public class Constant {

    public static final int STORAGE_PERMISSION_CODE = 3004;
    public static String USER_TOKEN = "UserToken";
    public static String USER_APP_VERSION = "UserAppVersion";
    public static String IS_USER_LOGIN = "IsUserLogin";
    public static String USER_NAME = "UserName";
    public static String USER_NUMBER = "UserNumber";
    public static String USER_EMAIL = "UserEmail";
    public static String HQ = "HQ";
    public static String REPORTING_MANAGER = "ReportingManager";
    public static String ADD_EDIT_DOCTOR = "AddEditDoctor";

    public static String ADD_DOCTOR = "Add";
    public static String EDIT_DOCTOR = "Edit";
}
