package com.techizer.hetro.doctorposter.helpers.utils;

import android.annotation.SuppressLint;
import android.text.TextUtils;

import java.util.regex.Pattern;

/**
 * Created by DhavalJayani on 16-07-2018.
 */

public class StringUtils {
    //static Pattern object, since pattern is fixed
    private static Pattern pattern;

    /**
     * Returns true if the string is null or 0-length.
     *
     * @param str the string to be examined
     * @return true if str is null or zero length
     */
    public static boolean isEmpty(CharSequence str) {
        return TextUtils.isEmpty(str);
    }

    /**
     * String to integer
     *
     * @param str
     * @param defValue
     * @return
     */
    public static int toInt(String str, int defValue) {
        try {
            return Integer.parseInt(str);
        } catch (Exception e) {
        }
        return defValue;
    }

    /**
     * Cuts the string at the end if it's longer than maxLength and appends "..." to it. The length of the resulting
     * string including "..." is always less or equal to the given maxLength. It's valid to pass a null text; in this
     * case null is returned.
     */
    public static String ellipsize(String text, int maxLength) {
        return ellipsize(text, maxLength, "...");
    }

    /**
     * Cuts the string at the end if it's longer than maxLength and appends the given end string to it. The length of
     * the resulting string is always less or equal to the given maxLength. It's valid to pass a null text; in this
     * case null is returned.
     */
    public static String ellipsize(String text, int maxLength, String end) {
        if (text != null && text.length() > maxLength) {
            return text.substring(0, maxLength - end.length()) + end;
        }
        return text;
    }


    public static String removeLastChar(String stringText, String endingChar) {
        if (!stringText.equals("") && stringText != null) {
            if (stringText.endsWith(endingChar)) {
                stringText = stringText.substring(0, stringText.length() - 1);
            }
        }
        return stringText;
    }

    @SuppressLint("DefaultLocale")
    public static String MonthConvert(int no) {
        return String.format("%02d", no);
    }

    //Function to convert one digit time into two digit Eg:9 minute become 09

    public static String twoDigitString(long number) {
        if (number == 0) {
            return "00";
        }
        if (number / 10 == 0) {
            return "" + number;
        }
        return String.valueOf(String.format("%02d", number));
    }

}
