package com.techizer.hetro.doctorposter.app;

import android.app.Application;
import android.content.Context;

public class AppController extends Application {

    private static Context context;
    private static AppController mInstance;

    public static AppController getInstance() {
        return mInstance;
    }

    public static Context getAppContext() {
        return AppController.context;
    }

    public void onCreate() {
        super.onCreate();
        mInstance = this;
        AppController.context = getApplicationContext();
    }
}



