package com.techizer.hetro.doctorposter.helpers.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by DhavalJayani on 19-07-2018.
 */

public class ValidatorUtils {
    //static Pattern object, since pattern is fixed
    private static Pattern pattern;

    //non-static Matcher object because it's created from the input String
    private static Matcher matcher;

    /**
     * This method validates the input email address with EMAIL_REGEX pattern
     *
     * @param email
     * @return boolean
     */
    public static boolean isEmail(String email) {
        pattern = Pattern.compile(RegexUtils.EMAIL_REGEX);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    /**
     * This method validates the input Full Name  with REGEX pattern
     *
     * @param str
     * @return boolean
     * exaple of full name pattern is Abc Xyz where Abc is First name and Xyz is Last name
     */
    public static boolean isFullname(String str) {
        return str.matches(RegexUtils.FULLNAME_REGEX);
    }

    /**
     * This method validates the input mobile number with REGEX pattern
     *
     * @param mobileNo
     * @return boolean
     * exaple of 8792541347 if it is this pattern then return true else false will be return.
     */
    public static boolean isMobileNumber(String mobileNo) {
        //validate phone numbers of format "1234567890". If it contains any "-" or space then it will return false else true
        if (mobileNo.matches(RegexUtils.MOBILE_LANDLINE_NUMBER_REGEX)) {
            return true;
        } else {
            return false;
        }
    }

    /*
     * Validating the password by matching the patterns.
     * Pattern contains the Passord as there is to be a number small letters Capital letters and special charactes as specified ( @#$%^&+= ) and
     * password length should be more than 7 character.
     * It will return true if pattern matches else it will return false.
     * */

    public static boolean isValidPassword(String password) {
        if (password.matches(RegexUtils.PASSWORD_REGEX)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * This method validates the input age with REGEX pattern
     * <p>
     * Here we are taking age between 1-99 consideriing max age of human as 99. we can also input the age as follow.
     * Eg. : 05,12. but not as 005. Also 0 is not a valid date as per RegexUtils pattern
     *
     * @param age
     * @return boolean
     */
    public static boolean isValidAge(String age) {
        matcher = pattern.matcher(age);
        return matcher.matches();
    }

    /**
     * This method validates the input url address with REGEX pattern
     *
     * @param url
     * @return boolean
     */
    public static boolean isValidURL(String url) {
        matcher = pattern.matcher(url);
        return matcher.matches();
    }

    /**
     * This method validates the input pin code(zip code) with REGEX pattern
     * Only 6 consicutive digit is allowd no white space,digit and any special symbol is allowed.
     *
     * @param pin
     * @return boolean
     */
    public static boolean isValidZipCode(String pin) {
        matcher = pattern.matcher(pin);
        return matcher.matches();
    }
}
