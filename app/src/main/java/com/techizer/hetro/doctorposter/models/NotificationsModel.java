package com.techizer.hetro.doctorposter.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class NotificationsModel {
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data = null;
    @SerializedName("error")
    @Expose
    private String error;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public static class Notifications {
        @SerializedName("notification_id")
        @Expose
        private String notificationId;

        @SerializedName("desc")
        @Expose
        private String notificationTitle;

        @SerializedName("date")
        @Expose
        private String notificationDate;

        @SerializedName("doctor_id")
        @Expose
        private String doctorId;

        public String getNotificationId() {
            return notificationId;
        }

        public void setNotificationId(String notificationId) {
            this.notificationId = notificationId;
        }

        public String getNotificationTitle() {
            return notificationTitle;
        }

        public void setNotificationTitle(String notificationTitle) {
            this.notificationTitle = notificationTitle;
        }

        public String getNotificationDate() {
            return notificationDate;
        }

        public void setNotificationDate(String notificationDate) {
            this.notificationDate = notificationDate;
        }

        public String getDoctorId() {
            return doctorId;
        }

        public void setDoctorId(String doctorId) {
            this.doctorId = doctorId;
        }
    }

    public class Data {
        @SerializedName("notification")
        @Expose
        private ArrayList<Notifications> notifications = null;

        public ArrayList<Notifications> getNotifications() {
            return notifications;
        }

        public void setNotifications(ArrayList<Notifications> notifications) {
            this.notifications = notifications;
        }
    }
}
