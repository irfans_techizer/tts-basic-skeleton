package com.techizer.hetro.doctorposter.helpers.utils;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by Dhaval Jayani on 10/4/19.
 */
public class ProgressUtils
{
    static ProgressDialog progressDialog;

    public static void ShowProgressDialog(Context context, String loaderMsg) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(loaderMsg);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public static void DismissProgressDialog(Context context) {
        progressDialog.dismiss();
    }
}
