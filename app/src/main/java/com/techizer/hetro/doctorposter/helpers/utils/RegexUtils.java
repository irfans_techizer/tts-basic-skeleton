package com.techizer.hetro.doctorposter.helpers.utils;

/**
 * Created by DhavalJayani on 18-07-2018.
 */

public class RegexUtils {

    public static final String MOBILE_NUMBER_REGEX = "\\d{10}";
    public static final String AGE_REGEX = "^([1-9][0-9][0-9]?|)$";
    public static final String FILENAME_REGEX = "^[^\\/?\"*:<>\\]{1,255}$";
    public static final String FULLNAME_REGEX = "^([A-Z][a-z]*((\\s)))+[A-Z][a-z]*$";
    public static final String PASSWORD_REGEX = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}";
    public static final String MOBILE_LANDLINE_NUMBER_REGEX = "((\\+*)((0[ -]+)*|(91 )*)(\\d{12}+|\\d{10}+))|\\d{5}([- ]*)\\d{6}";
    public static final String EMAIL_REGEX = "^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$";
}
