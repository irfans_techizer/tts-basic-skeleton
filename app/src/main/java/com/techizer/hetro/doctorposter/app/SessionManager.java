package com.techizer.hetro.doctorposter.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.techizer.hetro.doctorposter.BuildConfig;
import com.techizer.hetro.doctorposter.R;
import com.techizer.hetro.doctorposter.activity.LoginActivity;
import com.techizer.hetro.doctorposter.activity.MainActivity;
import com.techizer.hetro.doctorposter.models.LoginModel;

public class SessionManager {

    public static final String sharedPrefName = BuildConfig.APPLICATION_ID + "_session";
    public static final String prefFcmToken = BuildConfig.APPLICATION_ID + "_fcm_token";

    private static final String IS_LOGIN = "isLogin";
    private static final String prefToken = "userLoginToken";
    private static final String prefLoginModel = "loginModelData";

    private Context context;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    public SessionManager(Context context) {
        this.context = context;
        pref = context.getSharedPreferences(sharedPrefName, Context.MODE_PRIVATE);
        editor = pref.edit();
        editor.apply();
    }

    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }

    public void logoutUser() {
        editor.putBoolean(IS_LOGIN, false);
        editor.commit();

        Intent i = new Intent(context, LoginActivity.class);
        // i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        // activity.finishAffinity();
        context.startActivity(i);
    }

    public void logoutUser_Clear() {
        // Clearing all data from Shared Preferences
        java.net.CookieHandler.setDefault(null);
        editor.clear();
        editor.commit();

        Intent i = new Intent(context, LoginActivity.class);
        // i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        // activity.finishAffinity();
        context.startActivity(i);
    }

    public void CheckLoggedIn(Activity activity) {
        if (!this.isLoggedIn()) {
            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(activity, LoginActivity.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            // add new Flag to start new Activity
            // i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            // Staring Login Activity
            activity.finish();
            activity.startActivity(i);
            activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        } else {
            Intent i = new Intent(activity, MainActivity.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            // add new Flag to start new Activity
            // i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            // Staring Main Activity
            activity.finish();
            activity.startActivity(i);
            activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        }
    }

    public LoginModel.Data getLoginData() {
        String fromJson = pref.getString(prefLoginModel, null);
        return new Gson().fromJson(fromJson, LoginModel.Data.class);
    }

    public void setLoginData(LoginModel.Data data) {
        String toJsonData = new Gson().toJson(data);
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(prefToken, data.getToken());
        editor.putString(prefLoginModel, toJsonData);
        editor.commit();
    }

    public String getLoginAccessToken() {
        return pref.getString(prefToken, null);
    }

    public String getStrDataOf(String prefKeyName) {
        return pref.getString(prefKeyName, "");
    }

    public void setDataOf(String prefKeyName, String prefValue) {
        editor.putString(prefKeyName, !prefValue.isEmpty() ? prefValue : "");
        editor.commit();
    }

    public int getIntDataOf(String prefKeyName) {
        return pref.getInt(prefKeyName, 0);
    }

    public void setDataOf(String prefKeyName, int prefValue) {
        editor.putInt(prefKeyName, prefValue > 0 ? prefValue : 0);
        editor.commit();
    }

    public boolean getBoolDataOf(String prefKeyName) {
        return pref.getBoolean(prefKeyName, false);
    }

    public void setDataOf(String prefKeyName, boolean prefValue) {
        editor.putBoolean(prefKeyName, prefValue);
        editor.commit();
    }
}
