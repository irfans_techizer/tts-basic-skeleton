package com.techizer.hetro.doctorposter.helpers.utils;

import android.annotation.SuppressLint;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by DhavalJayani on 16-07-2018.
 */
public class DateUtils {

    public static Date stringToDate(String dateStr, String pattern) throws Exception {
        return new SimpleDateFormat(pattern, Locale.getDefault()).parse(dateStr);
    }

    public static String dateToString(Date date, String pattern) {
        return new SimpleDateFormat(pattern, Locale.getDefault()).format(date);
    }

    /**
     * get difference day
     *
     * @param startDate
     * @param endDate
     * @return
     */
    public static long getDifferenceDay(Date startDate, Date endDate) {
        // milliseconds
        long different = endDate.getTime() - startDate.getTime();

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        System.out.printf("%d days, %d hours, %d minutes, %d seconds%n",
                elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds);

        return elapsedDays; // returning days instead
    }

    /**
     * get Today date
     *
     * @return
     */
    @SuppressLint("SimpleDateFormat")
    public static String getTodayDate() {
        String sTodayDate;
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        sTodayDate = df.format(calendar.getTime());
        return sTodayDate;
    }

    /**
     * get current time
     *
     * @return
     */
    @SuppressLint("SimpleDateFormat")
    public static String getCurrentTime() {
        Date d = new Date();
        SimpleDateFormat output = new SimpleDateFormat("hh:mm", Locale.getDefault());
        return output.format(d);
    }

    /**
     * getCurrentYear
     *
     * @param date Date对象
     * @return 年
     */
    public static String getCurrentYear(Date date) {
        Date d = new Date();
        SimpleDateFormat output = new SimpleDateFormat("yyyy", Locale.getDefault());
        return output.format(d);
    }

    public static String getCurrentDate_Variable(String dateFormat, String whichVar) throws Exception {
        Date d = stringToDate(getTodayDate(), dateFormat);
        SimpleDateFormat output = new SimpleDateFormat(whichVar, Locale.getDefault());
        return output.format(d);
    }

    /**
     * get Month convert to two digit
     *
     * @param month
     * @return
     */
    public static String getMonthConvert(int month) {
        if (month < 10) {
            return "0" + month;
        } else {
            return "" + month;
        }
    }

    public static String parseDateTime(String dateString, String originalFormat, String outputFromat) {
        SimpleDateFormat formatter = new SimpleDateFormat(originalFormat, Locale.US);
        Date date;
        try {
            date = formatter.parse(dateString);
            SimpleDateFormat dateFormat = new SimpleDateFormat(outputFromat, new Locale("US"));
            return dateFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getRelativeTimeSpan(String dateString, String originalFormat) {
        SimpleDateFormat formatter = new SimpleDateFormat(originalFormat, Locale.US);
        Date date;
        try {
            date = formatter.parse(dateString);
            return android.text.format.DateUtils.getRelativeTimeSpanString(date.getTime()).toString();
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }
}
