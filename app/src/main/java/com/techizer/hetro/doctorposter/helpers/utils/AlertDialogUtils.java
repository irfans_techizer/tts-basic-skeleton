package com.techizer.hetro.doctorposter.helpers.utils;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.techizer.hetro.doctorposter.R;
import com.techizer.hetro.doctorposter.activity.LoginActivity;

public class AlertDialogUtils {

    public static String IS_USER_LOGIN = "IsUserLogin";
    static SharedPreferences.Editor editor;

    public static void showDialog(final Context context, String title, String message, final IYesDialog iYesDialog,
                                  final INoDialog iNoDialog,
                                  final boolean isSetCancelable, final boolean isCancelVisible, String buttonOk,
                                  String buttonCancel) {
        TextView txtViewOk, txtViewDescription, txtView_Title, txtViewCancle;
        View viewCancle;

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.template_error_dialog);
        dialog.setCancelable(isSetCancelable);
        dialog.setCanceledOnTouchOutside(false);

        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        //dialog.getWindow().setLayout(RelativeLayout.LayoutParams.FILL_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        txtViewOk = dialog.findViewById(R.id.txtViewOk);
        txtViewDescription = dialog.findViewById(R.id.txtViewDescription);
        txtView_Title = dialog.findViewById(R.id.txtView_Title);
        txtViewCancle = dialog.findViewById(R.id.txtViewCancle);
        viewCancle = dialog.findViewById(R.id.viewCancle);

        txtViewOk.setText(buttonOk);
        txtViewCancle.setText(buttonCancel);

        if (isCancelVisible) {
            txtViewCancle.setVisibility(View.VISIBLE);
            viewCancle.setVisibility(View.VISIBLE);
        } else {
            txtViewCancle.setVisibility(View.GONE);
            viewCancle.setVisibility(View.GONE);
        }

        txtViewOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (iYesDialog != null) {
                    dialog.dismiss();
                    iYesDialog.DialogYesCallback();
                } else {
                    dialog.dismiss();
                }
            }
        });

        txtViewCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (iNoDialog != null) {
                    dialog.dismiss();
                    iNoDialog.DialogNoCallback();
                } else {
                    dialog.dismiss();
                }
            }
        });
        txtView_Title.setText(title);
        txtViewDescription.setText(Html.fromHtml(message));

        dialog.show();
    }

    public static void dialogForErrorAuthentication(final Context context, String errorMessage,
                                                    final SharedPreferences sharedPreferences) {
        TextView txtViewOk, txtViewDescription, txtView_Title;

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.template_authentication_dialog_error);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(false);

        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        //dialog.getWindow().setLayout(RelativeLayout.LayoutParams.FILL_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);

        txtViewOk = dialog.findViewById(R.id.txtViewOk);
        txtViewDescription = dialog.findViewById(R.id.txtViewDescription);
        txtView_Title = dialog.findViewById(R.id.txtView_Title);

        txtViewOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                editor = sharedPreferences.edit();
                // editor.putBoolean(SessionManager.IS_LOGIN, false);
                editor.putBoolean(IS_USER_LOGIN, false);
                editor.apply();
                editor.commit();

                Intent startMain = new Intent(context, LoginActivity.class);
                startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                context.startActivity(startMain);
            }
        });

        txtView_Title.setText("Message");
        txtViewDescription.setText(Html.fromHtml(errorMessage));

        dialog.show();
    }

    public interface IYesDialog {
        void DialogYesCallback();
    }

    public interface INoDialog {
        void DialogNoCallback();
    }

    public interface IPictureDialog {
        void CameraPhotoCallback();

        void GalleryPhotoCallback();
    }
}
