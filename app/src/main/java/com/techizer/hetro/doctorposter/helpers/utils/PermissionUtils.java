package com.techizer.hetro.doctorposter.helpers.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.techizer.hetro.doctorposter.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PermissionUtils {

    Context context;
    Activity current_activity;

    PermissionResultCallback permissionResultCallback;
    ArrayList<String> permission_list = new ArrayList<>();
    ArrayList<String> listPermissionsNeeded = new ArrayList<>();
    String dialog_content = "";
    int req_code;
    int click_code;
    private boolean shouldFinishCurrentActivity = false;

    public PermissionUtils(Context context) {
        this.context = context;
        this.current_activity = (Activity) context;
        this.shouldFinishCurrentActivity = false;
        permissionResultCallback = (PermissionResultCallback) context;


    }

    public PermissionUtils(Context context, boolean shouldFinishCurrentActivity) {
        this.context = context;
        this.current_activity = (Activity) context;
        this.shouldFinishCurrentActivity = shouldFinishCurrentActivity;

        permissionResultCallback = (PermissionResultCallback) context;

    }


    public PermissionUtils(Context context, PermissionResultCallback callback) {
        this.context = context;
        this.current_activity = (Activity) context;

        permissionResultCallback = callback;


    }


    /**
     * Check the API Level & Permission
     *
     * @param permissions
     * @param dialog_content
     * @param request_code
     */

    public void check_permission(ArrayList<String> permissions, String dialog_content, int request_code, int click_code) {
        this.permission_list = permissions;
        this.dialog_content = dialog_content;
        this.req_code = request_code;
        this.click_code = click_code;

        if (Build.VERSION.SDK_INT >= 23) {
            if (checkAndRequestPermissions(permissions, request_code)) {
                permissionResultCallback.PermissionGranted(request_code, click_code);
                Log.i("all permissions", "granted");
                Log.i("proceed", "to callback");
            }
        } else {
            permissionResultCallback.PermissionGranted(request_code, click_code);

            Log.i("all permissions", "granted");
            Log.i("proceed", "to callback");
        }

    }


    /**
     * Check and request the Permissions
     *
     * @param permissions
     * @param request_code
     * @return
     */

    private boolean checkAndRequestPermissions(ArrayList<String> permissions, int request_code) {

        if (permissions.size() > 0) {
            listPermissionsNeeded = new ArrayList<>();

            for (int i = 0; i < permissions.size(); i++) {
                int hasPermission = ContextCompat.checkSelfPermission(current_activity, permissions.get(i));

                if (hasPermission != PackageManager.PERMISSION_GRANTED) {
                    listPermissionsNeeded.add(permissions.get(i));
                }

            }

            if (!listPermissionsNeeded.isEmpty()) {
                ActivityCompat.requestPermissions(current_activity, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), request_code);
                return false;
            }
        }

        return true;
    }


    /**
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        if (grantResults.length > 0) {
            Map<String, Integer> perms = new HashMap<>();

            for (int i = 0; i < permissions.length; i++) {
                perms.put(permissions[i], grantResults[i]);
            }

            final ArrayList<String> pending_permissions = new ArrayList<>();

            for (int i = 0; i < listPermissionsNeeded.size(); i++) {
                if (perms.size() > 0 && perms.get(listPermissionsNeeded.get(i)) != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(current_activity, listPermissionsNeeded.get(i)))
                        pending_permissions.add(listPermissionsNeeded.get(i));
                    else {

                        new PermissionDenyDialog(current_activity, "Permission required", dialog_content, new PermissionDenyDialog.OnButtonClickListner() {
                            @Override
                            public void okOKClicked() {

                                Intent intent = new Intent();
                                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package", current_activity.getPackageName(), null);
                                intent.setData(uri);
                                current_activity.startActivity(intent);
                                current_activity.finish();
                            }

                            @Override
                            public void canceledClicked() {

                                if (!shouldFinishCurrentActivity) {
                                    Log.i("Go to settings", "and enable permissions");
                                    permissionResultCallback.NeverAskAgain(req_code);

                                } else {
                                    current_activity.finish();
                                }
                            }
                        }).show();

                        return;

                    }


                }

            }

            if (pending_permissions.size() > 0) {


                new PermissionDenyDialog(context, "Permission required", dialog_content, new PermissionDenyDialog.OnButtonClickListner() {
                    @Override
                    public void okOKClicked() {

                        check_permission(permission_list, dialog_content, req_code, click_code);

                    }

                    @Override
                    public void canceledClicked() {

                        if (!shouldFinishCurrentActivity) {
                            Log.i("permisson", "not fully given");
                            if (permission_list.size() == pending_permissions.size())
                                permissionResultCallback.PermissionDenied(req_code);
                            else
                                permissionResultCallback.PartialPermissionGranted(req_code, pending_permissions);
                        } else {
                            current_activity.finish();
                        }
                    }
                }).show();

            } else {
                Log.i("all", "permissions granted");
                Log.i("proceed", "to next step");
                permissionResultCallback.PermissionGranted(req_code, click_code);

            }


        }

    }


    /**
     * Explain why the app needs permissions
     *
     * @param message
     * @param okListener
     */
    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(current_activity)
                .setMessage(message)
                .setPositiveButton("Ok", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show();
    }

    public interface PermissionResultCallback {
        void PermissionGranted(int request_code, int click_code);

        void PartialPermissionGranted(int request_code, ArrayList<String> granted_permissions);

        void PermissionDenied(int request_code);

        void NeverAskAgain(int request_code);
    }

    public static class PermissionDenyDialog extends Dialog {

        private String header, message;
        private PermissionDenyDialog.OnButtonClickListner buttonClickListner;
        private boolean onlyPositive = false, closeOnOutsideClick = true;
        private String negativeText;
        private String possitiveText;

        public PermissionDenyDialog(Context context, String header, String message,
                                    PermissionDenyDialog.OnButtonClickListner buttonClickListner) {
            super(context);
            this.header = header;
            this.message = message;
            this.buttonClickListner = buttonClickListner;
            negativeText = "";
            possitiveText = "";

        }

        public PermissionDenyDialog(Activity context, String header, String message,
                                    PermissionDenyDialog.OnButtonClickListner buttonClickListner, boolean onlyPositive) {
            super(context);
            this.header = header;
            this.message = message;
            this.buttonClickListner = buttonClickListner;
            this.onlyPositive = onlyPositive;
            negativeText = "";
            possitiveText = "";

        }


        public PermissionDenyDialog(Activity context, String header, String message,
                                    PermissionDenyDialog.OnButtonClickListner buttonClickListner, String negativeText, String posiitiveText, boolean onlyPositive, boolean closeOnOutsideClick) {
            super(context);
            this.header = header;
            this.message = message;
            this.buttonClickListner = buttonClickListner;
            this.onlyPositive = onlyPositive;
            this.negativeText = negativeText;
            this.possitiveText = posiitiveText;
            this.closeOnOutsideClick = closeOnOutsideClick;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            setContentView(R.layout.ok_cancel_custom_dialog_layout);

            this.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

            this.setCanceledOnTouchOutside(closeOnOutsideClick);
            this.setCancelable(false);

            TextView title = (TextView) findViewById(R.id.tvTitle);
            title.setText(header);


            TextView messageTV = (TextView) findViewById(R.id.tvMessage);
            messageTV.setMovementMethod(new ScrollingMovementMethod());
            messageTV.setText(Html.fromHtml(message));

            Button buttonOK = (Button) findViewById(R.id.bOk);
            if (!possitiveText.equalsIgnoreCase(""))
                buttonOK.setText(possitiveText);

            buttonOK.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    buttonClickListner.okOKClicked();
                    PermissionDenyDialog.this.dismiss();
                }
            });

            Button buttonCancel = (Button) findViewById(R.id.bCancel);
            if (onlyPositive) {
                buttonCancel.setVisibility(View.GONE);

            }


            if (!negativeText.equalsIgnoreCase(""))
                buttonCancel.setText(negativeText);

            buttonCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    buttonClickListner.canceledClicked();
                    PermissionDenyDialog.this.dismiss();
                }
            });

        }

        public interface OnButtonClickListner {

            void okOKClicked();

            void canceledClicked();
        }
    }
}


