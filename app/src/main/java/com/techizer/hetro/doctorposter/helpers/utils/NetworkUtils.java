package com.techizer.hetro.doctorposter.helpers.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import static android.content.Context.CONNECTIVITY_SERVICE;

/**
 * Created by Dhaval Jayani on 10/4/19.
 */
public class NetworkUtils
{
    public static boolean checkNetworkConnections(Context context) {
        boolean chk = true;
        ConnectivityManager mConnectionManager = (ConnectivityManager) context.getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo i = mConnectionManager.getActiveNetworkInfo();
        if (i == null) {
            chk = false;
        } else if (!i.isConnected() || !i.isAvailable()) {
            chk = false;
        }
        return chk;

    }
}
