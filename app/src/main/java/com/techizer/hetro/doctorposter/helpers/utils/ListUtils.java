package com.techizer.hetro.doctorposter.helpers.utils;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by DhavalJayani on 17-07-2018.
 */

public class ListUtils {
    /**
     * get index using binary search
     *
     * @param stringList
     * @param searchKey
     * @return
     */
    public static int getIndexUsingBinarySearch(List<String> stringList, String searchKey) {
        Comparator<String> comparator = new Comparator<String>() {
            public int compare(String s1, String s2) {
                return s1.compareTo(s2);
            }
        };

        int index = Collections.binarySearch(stringList,
                searchKey, comparator);

        return index;
    }
}
