package com.techizer.hetro.doctorposter.notification;

import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationChannelGroup;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.techizer.hetro.doctorposter.BuildConfig;
import com.techizer.hetro.doctorposter.R;
import com.techizer.hetro.doctorposter.activity.LoginActivity;
import com.techizer.hetro.doctorposter.activity.MainActivity;
import com.techizer.hetro.doctorposter.app.SessionManager;

import org.jetbrains.annotations.NotNull;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.Random;

public class FcmMessagingServices extends FirebaseMessagingService {

    private static final String TAG = "--> FcmMessaging";
    private static final String APP_NOTIFICATION_CHANNEL = BuildConfig.APPLICATION_ID + "_notifications_channel";
    private static final String APP_CHANNEL_GROUPS_NOTIFICATIONS = BuildConfig.APPLICATION_ID + "_notifications_group";

    private static Uri NOTIFICATION_SOUND_URI = Uri.parse("android.resource://com.techizer.hetro.doctorposter/" + R.raw.to_the_point);

    public static void setupNotificationChannels(Activity activity) {
        // Caution: You should guard this code with a condition on the SDK_INT version to run only on
        // Android 8.0 (API level 26) and higher, because the notification channels
        // APIs are not available in the support library.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            AudioAttributes audioAttributes = new AudioAttributes.Builder().setUsage(AudioAttributes.USAGE_NOTIFICATION_EVENT).build();

            NotificationManager notificationManager = (NotificationManager) activity.getSystemService(Context.NOTIFICATION_SERVICE);
            if (notificationManager != null) {
                notificationManager.createNotificationChannelGroup(new NotificationChannelGroup(
                        APP_CHANNEL_GROUPS_NOTIFICATIONS, activity.getString(R.string.notification_group_notifications)));

                NotificationChannel notificationChannel = new NotificationChannel(APP_NOTIFICATION_CHANNEL,
                        activity.getString(R.string.notification_channel_others), NotificationManager.IMPORTANCE_DEFAULT);
                notificationChannel.enableLights(true);
                notificationChannel.enableVibration(true);
                notificationChannel.setGroup(APP_CHANNEL_GROUPS_NOTIFICATIONS);
                notificationChannel.setSound(NOTIFICATION_SOUND_URI, audioAttributes);
                notificationChannel.setDescription(activity.getString(R.string.notification_desc_notifications));
                notificationChannel.setLockscreenVisibility(NotificationCompat.VISIBILITY_PUBLIC);
                notificationChannel.setLightColor(ContextCompat.getColor(activity, R.color.colorPrimary));
                notificationManager.createNotificationChannel(notificationChannel);
            }
        }
    }

    @Override
    public void onNewToken(@NotNull String newToken) {
        super.onNewToken(newToken);
        new SessionManager(getApplicationContext()).setDataOf(SessionManager.prefFcmToken, newToken);
        Log.e(TAG, newToken);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        System.out.println(TAG + " remoteMessage");

        if (remoteMessage.getData().size() > 0) {
            // String[] message = remoteMessage.getNotification().getBody().split("@:");
            // String title = remoteMessage.getNotification().getTitle();
            // String body = remoteMessage.getNotification().getBody();
            // String tag = remoteMessage.getNotification().getTag();
            // System.out.println("-->message: " + Arrays.toString(message));
            // goToPage(title, body, DrawerNavigationActivity.class);

            // String title = remoteMessage.getData().get("title").trim();
            // String body = remoteMessage.getData().get("body").trim();
            // String tag = remoteMessage.getData().get("tag").trim();
            // System.out.println("--> title: " + title);
            // System.out.println("--> body: " + body);
            // System.out.println("--> tag: " + tag);

            Map<String, String> notificationData = remoteMessage.getData();
            System.out.println("--> notificationData : " + notificationData);
            setupNotification(notificationData);
        }
    }

    // public void setupNotification(String title, String message, String tag) {
    private void setupNotification(Map<String, String> notificationData) {

        Date currentTime = Calendar.getInstance().getTime();
        int random = new Random().nextInt(500 - 100) + 100;
        int notificationId = random + currentTime.getMinutes() + currentTime.getHours() + currentTime.getSeconds() + 919;
        Log.e("notificationId ", "--> " + notificationId);

        String dataDesc = notificationData.get("body");
        String dataTitle = notificationData.get("title");
        String dataDate = notificationData.get("insert_dt"); // TODO -- need to confirm from where this date is coming from.

        Intent fallbackIntent = new Intent(this, MainActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(fallbackIntent);

        Class activityClass = new SessionManager(this).isLoggedIn() ? MainActivity.class : LoginActivity.class;

        Intent endPageIntent = new Intent(this, activityClass);
        endPageIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        endPageIntent.putExtra("notificationId", notificationId);
        endPageIntent.putExtra("fromNotification", true);
        endPageIntent.putExtra("description", dataDesc);
        endPageIntent.putExtra("title", dataTitle);
        endPageIntent.putExtra("date", dataDate);
        stackBuilder.addNextIntent(endPageIntent);

        // .setDefaults(Notification.DEFAULT_SOUND)
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, APP_NOTIFICATION_CHANNEL);
        notificationBuilder.setColorized(true)
                .setAutoCancel(true)
                .setContentText(dataDesc)
                .setContentTitle(dataTitle)
                .setSound(NOTIFICATION_SOUND_URI)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setChannelId(APP_NOTIFICATION_CHANNEL)
                .setVibrate(new long[]{700, 700, 700, 700})
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setCategory(NotificationCompat.CATEGORY_REMINDER)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(dataDesc))
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                .setContentIntent(stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT));

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            try {
                RingtoneManager.getRingtone(getApplicationContext(), NOTIFICATION_SOUND_URI).play();
            } catch (Exception e) {
                e.printStackTrace();
            }

            notificationManager.notify(notificationId, notificationBuilder.build());
        }
    }
}
