package com.techizer.hetro.doctorposter.helpers.utils;

import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import androidx.appcompat.app.AlertDialog;

import com.techizer.hetro.doctorposter.R;

import java.util.List;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Dhaval Jayani on 29/6/19.
 */
public class NotificationUtils {
    public static boolean isFromNotification(Bundle fromnotification) {
        if (fromnotification != null) {
            return (fromnotification.getBoolean("isfromnotification", false)) ? true : false;
        }
        return false;
    }

    public static void addFromNotification(Intent intent) {
        intent.putExtra("isfromnotification", true);
    }

    public static void autostart(Context context) {
        Intent intent = new Intent();
        String message = "";
        if (Build.BRAND.equalsIgnoreCase("xiaomi")) {
            intent.setComponent(new ComponentName("com.miui.securitycenter", "com.miui.permcenter.autostart.AutoStartManagementActivity"));
            message = String.format("%s requires to be enabled in 'Autostart' to function properly.%n", context.getString(R.string.app_name));
        } else if (Build.BRAND.equalsIgnoreCase("Letv")) {
            intent.setComponent(new ComponentName("com.letv.android.letvsafe", "com.letv.android.letvsafe.AutobootManageActivity"));
            message = String.format("%s requires to be enabled in 'Autoboot' to function properly.%n", context.getString(R.string.app_name));
        } else if (Build.BRAND.equalsIgnoreCase("Honor")) {
            intent.setComponent(new ComponentName("com.huawei.systemmanager", "com.huawei.systemmanager.optimize.process.ProtectActivity"));
            message = String.format("%s requires to be enabled in 'Protected Apps' to function properly.%n", context.getString(R.string.app_name));
        } else if (Build.BRAND.equalsIgnoreCase("oppo")) {
            intent.setComponent(new ComponentName("com.coloros.safecenter", "com.coloros.safecenter.permission.startup.StartupAppListActivity"));
            message = String.format("%s requires to be enabled in 'Startup App' to function properly.%n", context.getString(R.string.app_name));
        } else if (Build.BRAND.equalsIgnoreCase("vivo")) {
            intent.setComponent(new ComponentName("com.vivo.permissionmanager", "com.vivo.permissionmanager.activity.BgStartUpManagerActivity"));
            message = String.format("%s requires to be enabled in 'Startup App' to function properly.%n", context.getString(R.string.app_name));
        }
        List<ResolveInfo> list = context.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        if (list.size() > 0) {
            ShowAlertforAutoStart(message, context, intent);
        }
    }

    private static void ShowAlertforAutoStart(String message, final Context context, final Intent callingIntent) {
        final SharedPreferences settings = context.getSharedPreferences("ProtectedApps", MODE_PRIVATE);
        final String saveIfSkip = "skipProtectedAppsMessage";
        boolean skipMessage = settings.getBoolean(saveIfSkip, false);
        if (!skipMessage) {
            final SharedPreferences.Editor editor = settings.edit();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View dialogView = inflater.inflate(R.layout.alert_checkbox, null);
            CheckBox checkBox = (CheckBox) dialogView.findViewById(R.id.chkAndroid);
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    editor.putBoolean(saveIfSkip, isChecked);
                    editor.apply();
                }
            });
            new AlertDialog.Builder(context).setTitle("Protected Apps").setMessage(message).setView(dialogView).setPositiveButton("Go to Settings", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    context.startActivity(callingIntent);
                }
            }).setNegativeButton(android.R.string.cancel, null).show();
        }
    }
}
