package com.techizer.hetro.doctorposter.activity;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.techizer.hetro.doctorposter.R;
import com.techizer.hetro.doctorposter.app.SessionManager;
import com.techizer.hetro.doctorposter.helpers.utils.AppUtils;

import java.util.Locale;

public class SplashActivity extends AppCompatActivity {

    private Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        activity = SplashActivity.this;
        // getWindow().setBackgroundDrawable(ContextCompat.getDrawable(activity, R.drawable.re_splashscreen));

        TextView textViewVersion = findViewById(R.id.textViewVersionSplash);
        textViewVersion.setText(String.format(Locale.getDefault(), "App Version: %d", AppUtils.getAppVersionCode(activity)));

        if (AppUtils.isRooted()) {
            // Do something to prevent run this app on the device
            finish();
        } else {
            // Do nothing and run app normally
            StartApp(3);
        }
    }

    public void StartApp(final long sleepFor) {
        // Create Thread that will sleep for n seconds
        Thread background = new Thread() {
            public void run() {
                try {
                    // Thread will sleep for n seconds
                    sleep(sleepFor * 1000);

                    //checking if user is already login then we showing dashboard directly else redirecting to login page
                    SessionManager sessionManager = new SessionManager(activity);
                    /*if user was logged-in previously then he will directly land on dashboard
                    else login screen will be displayed*/
                    sessionManager.CheckLoggedIn(activity);
                    /*
                                        if (sessionManager.isLoggedIn()) {
                                            Intent i = new Intent(activity, MainActivity.class); //MainActivity
                                            startActivity(i);
                                            finish();
                                        } else {
                                            Intent i = new Intent(activity, LoginActivity.class);
                                            startActivity(i);
                                            finish();
                                        }
                    */
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        // start thread
        background.start();
    }
}
