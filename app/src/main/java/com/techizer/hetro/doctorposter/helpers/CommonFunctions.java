package com.techizer.hetro.doctorposter.helpers;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.format.Formatter;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.techizer.hetro.doctorposter.R;

import java.io.ByteArrayOutputStream;
import java.lang.reflect.Field;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.content.ContentValues.TAG;
import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Parth on 14-06-2017.
 */

public class CommonFunctions {

    // RegexUtils pattern to valid email address
    private static final String EMAIL_REGEX = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";
    //User Name Expression
    private static final String USERNAME_PATTERN = "^[A-Za-z0-9\\s._-]{2,60}$";

    public static int REQUEST_AUTO_START_PROMPT = 1002;

    public static boolean isFromNotificationClick = false;
    public static boolean isFromNotification = false;
    public static String drMobileNo = "";
    public static String drName = "";
    public static String USER_COUNTRY = "UserCountry";
    public static String IS_ADMIN = "Admin";
    public static String USER_TERMS_CONDITION = "UserTerms";
    public static String USER_CASE_CONDITION = "UserCases";

    public static String USER_PIC = "UserPic";
    public static String USER_COUNTRY_NAME = "UserCountryName";

    public static boolean isStorageAllowed = false;
    public static boolean isCameraAllowed = false;

    public static String USER_TOKEN = "UserToken";
    public static String USER_ID = "UserId"; //Eg:1
    public static String User_Email = "UserEmail"; //Eg:abc@gmail.com
    public static String USER_NAME = "UserName"; //Eg: ABC
    public static String USER_MOBILE = "UserMobile"; //Eg: 8844779955
    public static String USER_TYPE = "UserType"; //Eg: PMT
    public static String IS_USER_LOGIN = "IsUserLogin";
    public static String USER_REQUEST_ID = "UserRequestId";
    public static String IS_FIRST_TIME_ENTER = "IsFirstTimeEnter";


    public static String NOTIFICATION_COUNT = "NotificationCount";

    public static String DOCTOR_DIGICODE = "DoctorDigicode";

    public static int bodyPartClick = 0;
    public static int count = 0;

    public static boolean isRefresh = false;
    public static boolean isEditRefresh = false;
    //Creating Progress dialog(Loader)
    static ProgressDialog progressDialog;
    //static Pattern object, since pattern is fixed
    private static Pattern pattern;
    //non-static Matcher object because it's created from the input String
    private static Matcher matcher;
    //boolean variable to store result of test
    boolean result;
    //Password validation RegexUtils
    String passwordPattern = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}";
    //URL Pattern to check IP address also
    //"(?i)^(?:(?:https?|ftp)://)(?:\\S+(?::\\S*)?@)?(?:(?!(?:10|127)(?:\\.\\d{1,3}){3})(?!(?:169\\.254|192\\.168)(?:\\.\\d{1,3}){2})(?!172\\.(?:1[6-9]|2\\d|3[0-1])(?:\\.\\d{1,3}){2})(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))|(?:(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)(?:\\.(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)*(?:\\.(?:[a-z\\u00a1-\\uffff]{2,}))\\.?)(?::\\d{2,5})?(?:[/?#]\\S*)?$";

    //"^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
    //"\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
    //Age validaton RegexUtils
    String ageValidationPattern = "^(0?[1-9]|[1-9][0-9])$";
    //It will check the pincode. If the entered pincode start with 0 then pattern is not valid else pattern is valid.
    //"^[1-9][0-9]{6}$";
    //URL Pattern
    String regexURL = "^(http://|https://)?(www.)?([a-zA-Z0-9]+).[a-zA-Z0-9]*.[a-z]{3}.?([a-z]+)?$";
    //ZipCode(Pin code) Pattern
    String zipCodePattern = "\\d{6}";

    /**
     * This method validates the input email address with EMAIL_REGEX pattern
     *
     * @param email
     * @return boolean
     */
    public static boolean validateEmail(String email) {
        pattern = Pattern.compile(EMAIL_REGEX);
        matcher = pattern.matcher(email);
        return matcher.matches();

        //return email.matches(EMAIL_REGEX);
    }

    /**
     * This method validates the input Full Name  with REGEX pattern
     *
     * @param str
     * @return boolean
     * exaple of full name pattern is Abc Xyz where Abc is First name and Xyz is Last name
     */
    public static boolean isFullname(String str) {
        String expression = "^([A-Z][a-z]*((\\s)))+[A-Z][a-z]*$";
        return str.matches(expression);
    }

    /**
     * This method validates the input Full Name  with REGEX pattern
     *
     * @param name
     * @return boolean
     * exaple of full name pattern is Abc007 where Abc007 is First name
     */
    public static boolean validateName(final String name) {
        pattern = Pattern.compile(USERNAME_PATTERN);
        matcher = pattern.matcher(name);
        return matcher.matches();
    }

    /**
     * This method validates the input mobile number with REGEX pattern
     *
     * @param phoneNo
     * @return boolean
     * exaple of 8792541347 if it is this pattern then return true else false will be return.
     */
    public static boolean validatePhoneNumber(String phoneNo) {
        //validate phone numbers of format "1234567890". If it contains any "-" or space then it will return false else true
        return phoneNo.matches("\\d{10}");

    }

    public static boolean isDestroy(Activity activity) {
        return activity == null || activity.isFinishing() || (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1 && activity.isDestroyed());
    }
    /*
     * Validating the password by matching the patterns.
     * Pattern contains the Passord as there is to be a number small letters Capital letters and special charactes as specified ( @#$%^&+= ) and
     * password length should be more than 7 character.
     * It will return true if pattern matches else it will return false.
     * */

    //Function to show the progress dialog(Loader)
    public static void ShowProgressDialog(Context context, String loaderMsg) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(loaderMsg);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public static void SetProgressMessage(String loaderMsg) {
        progressDialog.setMessage(loaderMsg);
    }

    //Function to remove dialog
    public static void DismissProgressDialog(Context context) {
        try {
            progressDialog.dismiss();

        } catch (Exception ex) {

        }

    }

    public static long printDifferenceDay(Date startDate, Date endDate) {

        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        System.out.println("startDate : " + startDate);
        System.out.println("endDate : " + endDate);
        System.out.println("different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        System.out.printf(
                "%d days, %d hours, %d minutes, %d seconds%n",
                elapsedDays,
                elapsedHours, elapsedMinutes, elapsedSeconds);

        return elapsedDays;//returning days instead

    }

    public static String MonthConvert(int no) {
        if (no < 10) {
            return "0" + no;
        } else {
            return "" + no;
        }
    }

    public static String getAndroidVersion() {

        return "" + Build.VERSION.RELEASE;
    }

    public static String getCurrentAndroidOS() {

        Field[] fields = Build.VERSION_CODES.class.getFields();
        String osName;
        if (Build.VERSION.SDK_INT >= 21) {
            osName = fields[Build.VERSION.SDK_INT + 1].getName();
        } else {
            osName = fields[Build.VERSION.SDK_INT].getName();
        }

        return "" + osName;
    }

    public static String getCurrentAndroid_Name() {
        Field[] fields = Build.VERSION_CODES.class.getFields();
        String osName = fields[Build.VERSION.SDK_INT].getName();
        return "" + osName;
    }

    public static int getAppVersionCode(Context c) {
        PackageManager manager = c.getPackageManager();
        String packageName = "";
        int versionCode = 0;
        try {
            PackageInfo info = manager.getPackageInfo(c.getPackageName(), 0);
            packageName = info.packageName;
            versionCode = info.versionCode;

        } catch (PackageManager.NameNotFoundException e) {
            // TODO Auto-generated catch block
        }
        return versionCode;
    }

    public static String GetAppVersionName(Context c) {
        PackageManager manager = c.getPackageManager();
        String packageName = "";
        String versionName = "";
        try {
            PackageInfo info = manager.getPackageInfo(c.getPackageName(), 0);
            packageName = info.packageName;
            versionName = info.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            // TODO Auto-generated catch block
        }
        return versionName;
    }

    public static String getMobileVersion() {

        return "" + Build.MANUFACTURER + " " + Build.MODEL;
    }

    public static void hideKeyboard(Activity activity) {
        View v = activity.getWindow().getCurrentFocus();
        if (v != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }
    }

    public static boolean appInstalledOrNot(String uri, Context ctx) {
        PackageManager pm = ctx.getPackageManager();
        boolean app_installed;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }

    public static void showInternetConnectivitystatus(Context ctx, boolean isConnected) {
        String message;
        if (!isConnected) {
            message = "Sorry! Not connected to internet";
            Toast.makeText(ctx, "" + message, Toast.LENGTH_SHORT).show();
        }


    }

    //getting the IPAddress of device
    public static String getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        String ip = Formatter.formatIpAddress(inetAddress.hashCode());
                        Log.i(TAG, "***** IP=" + ip);
                        return ip;
                    }
                }
            }
        } catch (SocketException ex) {
            Log.e(TAG, ex.toString());
        }
        return null;
    }

    /*
    // Method to manually check connection status
    public static boolean checkConnection(Context ctx) {
        boolean isConnected = ConnectivityReceiver.isConnected();
        showInternetConnectivitystatus(ctx, isConnected);

        return isConnected;
    }
    */

    public static String ConvertToBase64(Bitmap userPicBitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        userPicBitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
        //userPicBitmap.recycle();
        return encoded;
    }

    public static long byteToMB(long size) {

        //transform in MB
        long sizeInMb = size / (1024 * 1024);
        return sizeInMb;
    }

    public static List<String> splitWordsIntoStringsThatFit(String source, float maxWidthPx, Paint paint) {
        ArrayList<String> result = new ArrayList<>();

        ArrayList<String> currentLine = new ArrayList<>();

        String[] sources = source.split("\\s");
        for (String chunk : sources) {
            if (paint.measureText(chunk) < maxWidthPx) {
                processFitChunk(maxWidthPx, paint, result, currentLine, chunk);
            } else {
                //the chunk is too big, split it.
                List<String> splitChunk = splitIntoStringsThatFit(chunk, maxWidthPx, paint);
                for (String chunkChunk : splitChunk) {
                    processFitChunk(maxWidthPx, paint, result, currentLine, chunkChunk);
                }
            }
        }

        if (!currentLine.isEmpty()) {
            result.add(TextUtils.join(" ", currentLine));
        }
        return result;
    }

    /**
     * Splits a string to multiple strings each of which does not exceed the width
     * of maxWidthPx.
     */
    private static List<String> splitIntoStringsThatFit(String source, float maxWidthPx, Paint paint) {
        if (TextUtils.isEmpty(source) || paint.measureText(source) <= maxWidthPx) {
            return Arrays.asList(source);
        }

        ArrayList<String> result = new ArrayList<>();
        int start = 0;
        for (int i = 1; i <= source.length(); i++) {
            String substr = source.substring(start, i);
            if (paint.measureText(substr) >= maxWidthPx) {
                //this one doesn't fit, take the previous one which fits
                String fits = source.substring(start, i - 1);
                result.add(fits);
                start = i - 1;
            }
            if (i == source.length()) {
                String fits = source.substring(start, i);
                result.add(fits);
            }
        }

        return result;
    }

    /**
     * Processes the chunk which does not exceed maxWidth.
     */
    private static void processFitChunk(float maxWidth, Paint paint, ArrayList<String> result, ArrayList<String> currentLine, String chunk) {
        currentLine.add(chunk);
        String currentLineStr = TextUtils.join(" ", currentLine);
        if (paint.measureText(currentLineStr) >= maxWidth) {
            //remove chunk
            currentLine.remove(currentLine.size() - 1);
            result.add(TextUtils.join(" ", currentLine));
            currentLine.clear();
            //ok because chunk fits
            currentLine.add(chunk);
        }
    }

    public static boolean isFromNotification(Bundle fromnotification) {
        if (fromnotification != null) {
            return fromnotification.getBoolean("isfromnotification", false);
        }

        return false;

    }

    public static void addFromNotification(Intent intent) {
        intent.putExtra("isfromnotification", true);
    }


    public static void auto_start_prompt(Activity activity) {
        String message = "";
        Intent intent = new Intent();

        if (Build.BRAND.equalsIgnoreCase("xiaomi")) {
            intent.setComponent(new ComponentName("com.miui.securitycenter", "com.miui.permcenter.autostart.AutoStartManagementActivity"));
            message = String.format("%s requires to be enabled in 'Autostart' to function properly.%n", activity.getString(R.string.app_name));
        } else if (Build.BRAND.equalsIgnoreCase("letv")) {
            intent.setComponent(new ComponentName("com.letv.android.letvsafe", "com.letv.android.letvsafe.AutobootManageActivity"));
            message = String.format("%s requires to be enabled in 'Autoboot' to function properly.%n", activity.getString(R.string.app_name));
        } else if (Build.BRAND.equalsIgnoreCase("honor")) {
            intent.setComponent(new ComponentName("com.huawei.systemmanager", "com.huawei.systemmanager.optimize.process.ProtectActivity"));
            message = String.format("%s requires to be enabled in 'Protected Apps' to function properly.%n", activity.getString(R.string.app_name));
        } else if (Build.BRAND.equalsIgnoreCase("oppo")) {
            intent.setComponent(new ComponentName("com.coloros.safecenter", "com.coloros.safecenter.permission.startup.StartupAppListActivity"));
            message = String.format("%s requires to be enabled in 'Startup App' to function properly.%n", activity.getString(R.string.app_name));
        } else if (Build.BRAND.equalsIgnoreCase("vivo")) {
            intent.setComponent(new ComponentName("com.vivo.permissionmanager", "com.vivo.permissionmanager.activity.BgStartUpManagerActivity"));
            message = String.format("%s requires to be enabled in 'Startup App' to function properly.%n", activity.getString(R.string.app_name));
        }

        List<ResolveInfo> list = activity.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        if (list.size() > 0) ShowAlertAutoStart(message, activity, intent);
    }

    private static void ShowAlertAutoStart(String message, final Activity activity, final Intent callingIntent) {

        final String saveIfSkip = "skipProtectedAppsMessage";
        final SharedPreferences settings = activity.getSharedPreferences(
                activity.getPackageName() + "_ProtectedApps", MODE_PRIVATE);

        if (!settings.getBoolean(saveIfSkip, false)) {
            final SharedPreferences.Editor editor = settings.edit();
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View dialogView = inflater.inflate(R.layout.alert_checkbox, null);
            CheckBox checkBox = dialogView.findViewById(R.id.chkAndroid);

            checkBox.setOnCheckedChangeListener((buttonView, isChecked) -> {
                editor.putBoolean(saveIfSkip, isChecked);
                editor.apply();
            });

            new AlertDialog.Builder(activity)
                    .setMessage(message)
                    .setView(dialogView)
                    .setCancelable(false)
                    .setTitle("Protected Apps")
                    .setPositiveButton(R.string.settings, (dialog, which) ->
                            activity.startActivityForResult(callingIntent, REQUEST_AUTO_START_PROMPT)
                    )
                    .setNegativeButton(R.string.cancel, null)
                    .show();
        }
    }

    public boolean validatePassword(String password) {
        return password.matches(passwordPattern);
    }

    /**
     * This method validates the input age with REGEX pattern
     * <p>
     * Here we are taking age between 1-99 consideriing max age of human as 99. we can also input the age as follow.
     * Eg. : 05,12. but not as 005. Also 0 is not a valid date as per RegexUtils pattern
     *
     * @param age
     * @return boolean
     */
    public boolean validateAge(String age) {
        matcher = pattern.matcher(age);
        return matcher.matches();
    }

    /**
     * This method validates the input url address with REGEX pattern
     *
     * @param url
     * @return boolean
     */
    public boolean validateURL(String url) {
        matcher = pattern.matcher(url);
        return matcher.matches();

    }

    /**
     * This method validates the input pin code(zip code) with REGEX pattern
     * Only 6 consicutive digit is allowd no white space,digit and any special symbol is allowed.
     *
     * @param pin
     * @return boolean
     */
    public boolean validateZipCode(String pin) {
        matcher = pattern.matcher(pin);
        return matcher.matches();

    }

    /*Converting seconds into hours minutes and seconds*/
    public String getDurationString(long seconds) {

        long hours = seconds / 3600;
        long minutes = (seconds % 3600) / 60;
        seconds = seconds % 60;

        return twoDigitString(hours) + " hr : " + twoDigitString(minutes) + " min : " + twoDigitString(seconds) + " sec";
    }

    //Function to convert one digit time into two digit Eg:9 minute become 09
    private String twoDigitString(long number) {

        if (number == 0) {
            return "00";
        }

        if (number / 10 == 0) {
            return "0" + number;
        }

        return String.valueOf(number);
    }
}
