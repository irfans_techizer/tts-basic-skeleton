package com.techizer.hetro.doctorposter.helpers.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Dhaval Jayani on 8/4/19.
 */
public class TimeUtils {
    public static String parse24TimeTo12(String _24HourTime) {
        SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm");
        SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
        Date _24HourDt = null;
        try {
            _24HourDt = _24HourSDF.parse(_24HourTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
       /* System.out.println(_24HourDt);
            System.out.println(_12HourSDF.format(_24HourDt));*/

        return _12HourSDF.format(_24HourDt);
    }
}
