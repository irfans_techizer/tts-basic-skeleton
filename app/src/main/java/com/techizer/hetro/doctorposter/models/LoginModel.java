package com.techizer.hetro.doctorposter.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginModel {

    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("device_id")
    @Expose
    private String device_id;
    @SerializedName("device_type")
    @Expose
    private String device_type;
    @SerializedName("os")
    @Expose
    private String os;
    @SerializedName("device_name")
    @Expose
    private String device_name;
    @SerializedName("app_version")
    @Expose
    private String appVersion;

    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("error")
    @Expose
    private String error;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getDevice_name() {
        return device_name;
    }

    public void setDevice_name(String device_name) {
        this.device_name = device_name;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public class Data {

        @SerializedName("token")
        @Expose
        private String token;
        @SerializedName("user_id")
        @Expose
        private String user_id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("mobile_number")
        @Expose
        private String mobile;
        @SerializedName("hq")
        @Expose
        private String hq;
        @SerializedName("reporting_manager")
        @Expose
        private String reporting_manager;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("android_version")
        @Expose
        private String android_version;
        @SerializedName("type")
        @Expose
        private String login_type;

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getUserId() {
            return user_id;
        }

        public void setUserId(String user_id) {
            this.user_id = user_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAndroidVersion() {
            return android_version;
        }

        public void setAndroidVersion(String android_version) {
            this.android_version = android_version;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getHq() {
            return hq;
        }

        public void setHq(String hq) {
            this.hq = hq;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getLoginType() {
            return login_type;
        }

        public void setLoginType(String login_type) {
            this.login_type = login_type;
        }

        public String getReportingManager() {
            return reporting_manager;
        }

        public void setReportingManager(String reporting_manager) {
            this.reporting_manager = reporting_manager;
        }
    }
}
