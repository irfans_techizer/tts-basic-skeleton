package com.techizer.hetro.doctorposter.helpers.utils;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;

/**
 * Created by DhavalJayani on 19-07-2018.
 */

public class DisplayUtils {
    /**
     * Convert from dp units to px (pixels) according to the resolution of the phone
     *
     * @param dp
     * @return
     */
    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    /**
     * Convert Number to Dp
     */
    public static int NumberToDP(int number) {
        float scale = Resources.getSystem().getDisplayMetrics().density;

        return (int) (number * scale + 0.5f);
    }

    /**
     * Convert from px (pixels) to dp according to the resolution of the phone
     *
     * @param px
     * @return
     */

    public static int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }

    /**
     * Is it currently landscape
     *
     * @param context
     * @return
     */
    public static boolean isLandscape(Context context) {
        return context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
    }

    /**
     * Is it currently portrait
     *
     * @param context
     * @return
     */
    public static boolean isPortrait(Context context) {
        return context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;
    }
}
