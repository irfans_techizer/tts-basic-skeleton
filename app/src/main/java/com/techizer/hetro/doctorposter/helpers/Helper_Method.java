package com.techizer.hetro.doctorposter.helpers;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.IBinder;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.SpannableString;
import android.text.style.StrikethroughSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.widget.NestedScrollView;

import com.google.android.material.textfield.TextInputLayout;
import com.techizer.hetro.doctorposter.R;

import java.util.ArrayList;
import java.util.Objects;
import java.util.regex.Pattern;

import io.reactivex.disposables.Disposable;

public class Helper_Method {

    private static ProgressDialog dialog;

    public static String toTitleCase(String input) {
        boolean nextTitleCase = true;
        StringBuilder titleCase = new StringBuilder();

        if (!input.isEmpty()) {
            for (char c : input.toCharArray()) {
                if (Character.isSpaceChar(c))
                    nextTitleCase = true;
                else if (nextTitleCase) {
                    c = Character.toTitleCase(c);
                    nextTitleCase = false;
                }
                titleCase.append(c);
            }
        }
        return titleCase.toString();
    }

    public static void sout(Object object) {
        System.out.println("--->> " + object);
    }

    public static void sout(String label, Object object) {
        System.out.println("--->> " + label + "__ " + object);
    }

    public static boolean emailValidator(String email) {
        final String email_pattern = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        return Pattern.compile(email_pattern).matcher(email).matches();
    }

    public static void setSpinnerError(Spinner spinner, String prompt) {
        View selectedView = spinner.getSelectedView();
        if (selectedView != null && selectedView instanceof TextView) {
            TextView errorText = (TextView) selectedView;
            errorText.setError("");
            errorText.setTextColor(Color.RED); // just to highlight that this is an error
            errorText.setText(prompt); // changes the selected item text to this
        }
    }

    public static void setErrorBelow(TextInputLayout textInputLayout, String prompt) {
        textInputLayout.requestFocus(View.FOCUS_DOWN);
        textInputLayout.setErrorEnabled(true);
        textInputLayout.setError(prompt);
    }

    public static void setErrorBelow(EditText editText, String prompt) {
        editText.setError(prompt);
        editText.requestFocus();
    }

    public static void removeError(EditText editText) {
        editText.setError(null);
    }

    public static void removeError(TextInputLayout[] inputLayouts) {
        for (TextInputLayout view : inputLayouts) {
            view.setErrorEnabled(false);
            view.setError(null);
            view.clearFocus();
        }
    }

    public static void removeError(TextInputLayout inputLayouts) {
        inputLayouts.setErrorEnabled(false);
        inputLayouts.setError(null);
    }

    public static void removeError(TextInputLayout[] inputLayouts, TextView textView) {
        for (TextInputLayout view : inputLayouts) {
            view.setErrorEnabled(false);
            view.setError(null);
        }
        textView.setVisibility(View.GONE);
    }

    public static void smoothScrollTo(View view, NestedScrollView scrollView) {
        scrollView.smoothScrollTo(0, view.getTop());
    }

    public static void toaster(Activity activity, String content, int which) {
        Toast.makeText(activity, content, Toast.LENGTH_SHORT).show();
        // StyleableToast.makeText(activity, content, Toast.LENGTH_SHORT, which == 1 ? R.style.Info_Toast : R.style.Error_Toast).show();
    }

    public static void toaster_long(Activity activity, String content, int which) {
        Toast.makeText(activity, content, Toast.LENGTH_LONG).show();
        // StyleableToast.makeText(activity, content, Toast.LENGTH_LONG, which == 1 ? R.style.Info_Toast : R.style.Error_Toast).show();
    }

    // where first 0 shows the starting and data.length()/end shows the ending span.
    // if you want to span only part of it than you can change these values like 5, 8 then it will underline part of it.
    public static SpannableString underlineText(String data, int start, int end) {
        SpannableString content = new SpannableString(data);
        content.setSpan(new UnderlineSpan(), start, end, 0);
        return content;
    }

    public static SpannableString strikeoutText(String data, int start, int end) {
        SpannableString content = new SpannableString(data);
        content.setSpan(new StrikethroughSpan(), start, end, 0);
        return content;
    }

    public static void setFontAwesomeFace(Activity activity, View view) {
        Typeface typeface = Typeface.createFromAsset(activity.getAssets(), "font/fontawesome_webfont.ttf");
        if (view instanceof TextView) ((TextView) view).setTypeface(typeface);
        if (view instanceof EditText) ((EditText) view).setTypeface(typeface);
        if (view instanceof Button) ((Button) view).setTypeface(typeface);
    }

    public static String leadZero(int month) {
        return month < 10 ? "0" + month : "" + month;
    }

    public static long getDate(int d) {
        return 1000 * 60 * 60 * 24 * (d - 1);
    }

    public static void warnUser(Activity activity, String title, String message) {
        Log.e("warnUser: ", "" + title + ", " + message);
        new AlertDialog.Builder(activity)
                .setTitle(title)
                .setMessage(message)
                .show();
    }

    public static void warnUser(Activity activity, String title, String message, final boolean hideDialog) {
        Log.e("warnUser: ", "" + title + ", " + message);
        new AlertDialog.Builder(activity)
                .setTitle(title)
                .setMessage(message)
                .setOnCancelListener(dialog -> {
                    dialog.dismiss();
                    if (hideDialog) Helper_Method.hideDialog();
                })
                .show();
    }

    public static void warnUser_finish(final Activity activity, String title, String message, final boolean hideDialog) {
        Log.e("warnUser: ", "" + title + ", " + message);

        new AlertDialog.Builder(activity)
                .setIcon(R.drawable.ic_notifications)
                .setTitle(title)
                .setMessage(message)
                .setOnDismissListener(dialogInterface -> {
                    if (hideDialog) Helper_Method.hideDialog();
                    activity.finish();
                })
                .show();
    }

    public static void warnUser_exit(final Activity activity, String title, String message, final boolean hideDialog) {
        Log.e("warnUser: ", "" + title + ", " + message);

        new AlertDialog.Builder(activity)
                .setIcon(R.drawable.ic_notifications)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(R.string.ok, (dialog, i) -> dialog.dismiss())
                .setOnDismissListener(dialogInterface -> {
                    if (hideDialog) Helper_Method.hideDialog();

                    if (!activity.getClass().getName().endsWith("SplashActivity"))
                        activity.finish();
                    else
                        activity.finishAffinity();
                })
                .show();
    }

    private static void initDialog(Activity activity) {
        dialog = new ProgressDialog(activity);
        dialog.setMessage("processing... please wait !!");
        dialog.setCancelable(false);
    }

    private static void initDialog(Activity activity, String message) {
        dialog = new ProgressDialog(activity);
        dialog.setMessage(message);
        dialog.setCancelable(false);
    }

    public static void showDialog(Activity activity) {
        initDialog(activity);
        hideDialog();
        if (dialog != null && !dialog.isShowing()) dialog.show();
    }

    public static void showDialog(Activity activity, String message) {
        initDialog(activity, message);
        if (dialog != null && !dialog.isShowing()) dialog.show();
    }

    public static void hideDialog() {
        if (dialog != null && dialog.isShowing()) dialog.dismiss();
    }

    public static AlertDialog noConnectivityDialog(Activity activity, boolean closeApp) {
        return ConnectivityDialog(activity, closeApp, true);
    }

    public static AlertDialog noConnectivityDialog(Activity activity, boolean closeApp, boolean cancelable) {
        return ConnectivityDialog(activity, closeApp, cancelable);
    }

    private static AlertDialog ConnectivityDialog(final Activity activity, final boolean closeApp, final boolean cancelable) {
        return new AlertDialog.Builder(activity)
                .setCancelable(cancelable)
                .setTitle(R.string.unable_to_connect)
                .setIcon(R.drawable.ic_notifications)
                .setMessage(R.string.internet_connection_required)
                .setPositiveButton(R.string.settings, (dialog, id) -> activity.startActivity(new Intent(Settings.ACTION_SETTINGS)))
                .setNegativeButton(R.string.cancel, (dialog, id) -> {
                            Helper_Method.toaster(activity, activity.getString(R.string.internet_connection_required), 0);
                            if (closeApp) activity.finishAffinity();
                        }
                )
                .setOnDismissListener(dialogInterface -> {
                    Helper_Method.toaster(activity, activity.getString(R.string.internet_connection_required), 0);
                    if (closeApp) activity.finishAffinity();
                })
                .setCancelable(cancelable)
                .create();
    }

    public static void intentActivity(Activity activity, Class activityClass, Boolean finishActivity) {
        if (finishActivity) activity.finish();
        activity.startActivity(new Intent(activity, activityClass).putExtra("className", activity.getClass().getName()));
    }

    public static void intentActivity_animate(Activity activity, Class activityClass, Boolean finishActivity) {
        if (finishActivity) activity.finish();
        activity.startActivity(new Intent(activity, activityClass).putExtra("className", activity.getClass().getName()));
        activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    public static void finish_anim(Activity activity) {
        activity.finish();
        activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    @SuppressLint("ClickableViewAccessibility")
    public static void setupUI(View view, final Activity activity) {
        // Set up touch listener for non-text box views to hide keyboard
        if (!(view instanceof EditText)) {
            view.setOnTouchListener((v, event) -> {
                try {
                    InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
                    IBinder binder = Objects.requireNonNull(activity.getCurrentFocus()).getWindowToken();
                    inputMethodManager.hideSoftInputFromWindow(binder, 0);
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("--> Exception: " + e.getLocalizedMessage());
                }
                return false;
            });
        }

        // If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView, activity);
            }
        }
    }

    // compile 'com.github.pwittchen:reactivenetwork-rx2:0.12.1'
    public static void safelyDispose(Disposable... disposables) {
        for (Disposable subscription : disposables) {
            if (subscription != null && !subscription.isDisposed())
                subscription.dispose();
        }
    }

    public static String getNextArrayList(ArrayList<String> arrayList, String curr_recp) {
        int idx = arrayList.indexOf(curr_recp);
        if (idx < 0 || idx + 1 == arrayList.size()) return "";
        return arrayList.get(idx + 1);
    }

    public static String getPreviousArrayList(ArrayList<String> arrayList, String curr_recp) {
        int idx = arrayList.indexOf(curr_recp);
        if (idx <= 0) return "";
        return arrayList.get(idx - 1);
    }

/*
    public static String unescapeHtml(String escapedHtml) {
        String html = StringEscapeUtils.unescapeHtml4(escapedHtml);
        if (html.contains("&amp;")) html = html.replace("&amp;", "&");
        if (html.contains("&lt;")) html = html.replace("&lt;", "<");
        if (html.contains("&gt;")) html = html.replace("&gt;", ">");
        if (html.contains("&quot;")) html = html.replace("&quot;", "\"");
        if (html.contains("&amp;nbsp;")) html = html.replace("&amp;nbsp;", "&nbsp;");
        if (html.contains("&nbsp;")) html = html.replace("&nbsp;", " ");
        if (html.contains("#")) html = html.replace("#", "/-/");
        // this is used for parsing # in queryString
        return html;
    }
*/

    public static Point getDisplaySize(Display display) {
        Point size = new Point();
        display.getSize(size);
        return size;
    }

    public static int getIndexInSpinner(Spinner spinner, String myString) {
        int index = 0;
        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)) {
                index = i;
                break;
            }
        }
        return index;
    }

    public static String getUriFileName(Context context, Uri uri) {
        String result = null;
        String[] projection = {MediaStore.Images.Media.DATA};

        Cursor cursor = context.getContentResolver().query(uri, projection,
                null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                int column_index = cursor.getColumnIndexOrThrow(projection[0]);
                String resArr[] = cursor.getString(column_index).split("/");
                result = resArr[resArr.length - 1];
            }
            cursor.close();
        }

        if (result == null) result = "not_found";

        return result;
    }

    public static void setOverflowButton(final Activity activity) {
        final ViewGroup decorView = (ViewGroup) activity.getWindow().getDecorView();
        final ViewTreeObserver viewTreeObserver = decorView.getViewTreeObserver();
        viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                final ArrayList<View> outViews = new ArrayList<>();
                decorView.findViewsWithText(outViews, activity.getString(R.string.ab_more_options), View.FIND_VIEWS_WITH_CONTENT_DESCRIPTION);

                if (outViews.isEmpty()) return;

                AppCompatImageView overflow = (AppCompatImageView) outViews.get(0);
                overflow.setImageResource(R.drawable.ic_icon_listing);
                decorView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });
    }
}
